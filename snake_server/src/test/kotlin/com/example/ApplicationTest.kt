package com.example

import com.example.plugins.configureAdministration
import com.example.plugins.configureRouting
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.server.testing.*
import kotlin.test.*
import io.ktor.http.*

class ApplicationTest {
    @Test
    fun testCanShutdown() = testApplication {
        application {
            configureRouting()
            configureAdministration()
        }
        client.get("/ktor/application/shutdown").apply {
            assertEquals(HttpStatusCode.OK, status)
            assertEquals("", bodyAsText())
        }
    }
}
