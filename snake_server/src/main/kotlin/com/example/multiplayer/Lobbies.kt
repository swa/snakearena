package com.example.multiplayer

class Lobbies(private val lobbies: HashMap<Int, Lobby> = HashMap()) {

    companion object {
        const val maximumLobbies = 100
    }

    fun getLobby(id: Int) = lobbies[id]

    fun removeLobby(id: Int) = lobbies.remove(id)

    /**
     * Creates a new lobby and returns its id
     */
    fun createLobby(): Int? {
        return if (maximumLobbies > lobbies.size) {
            val id = newLobbyId()
            val lobby = Lobby(id)
            lobbies[id] = lobby

            id
        } else {
            null
        }
    }

    private fun newLobbyId(): Int {
        var id = 0

        while (lobbies.containsKey(id)) id += 1

        return id
    }

}