package com.example.multiplayer

import com.example.snakelib.net.ClientLobbyPacket
import com.example.snakelib.net.ClientPacket
import com.example.snakelib.net.ServerLobbyPacket
import kotlinx.coroutines.delay
import java.util.*
import java.util.logging.Logger

/**
 * A Lobby contains a list of players which will play a game together.
 *
 * TODO: Lobbies should probably get deleted after a while, if no game was started
 */
class Lobby(val id: Int, val players: MutableList<Player> = mutableListOf()) {
    fun add(player: Player): Boolean {
        if (players.any { it.name == player.name }) {
            Logger.getLogger("Lobby").info("Lobby $id: Not Connected ${player.name}")
            return false
        }
        players.add(player)
        Logger.getLogger("Lobby").info("Lobby $id: Connected ${player.name}")
        return true
    }

    suspend fun run(): Pair<Long, String> {
        while (true) {
            for (player in players) {
                for (incoming in player.incoming) {
                    if (incoming !is ClientPacket.LobbyPacket) {
                        continue
                    }
                    when (val lobbyPacket = incoming.lobbyPacket) {
                        // Nothing needs to be done, Lobby only contains connected players
                        is ClientLobbyPacket.Connect -> {}
                        is ClientLobbyPacket.StartGame -> {
                            val startTime = startGame()
                            Logger.getLogger("Lobby").info("Lobby $id is done")
                            return startTime to lobbyPacket.map
                        }
                    }
                }
                player.incoming.clear()
            }

            delay(100)
        }
    }

    private suspend fun startGame(): Long {
        val startDelayMs = 500
        val startTime = Date().time + startDelayMs
        for ((index, player) in players.withIndex()) {
            player.sendLobbyPacket(ServerLobbyPacket.StartGame(startTime, index))
        }
        return startTime
    }
}