package com.example.multiplayer

import com.example.snakelib.Game
import com.example.snakelib.GameRunner
import com.example.snakelib.Snake
import com.example.snakelib.Vec2d
import com.example.snakelib.game_field.GameFieldLoader
import com.example.snakelib.net.ClientGamePacket
import com.example.snakelib.net.ClientPacket
import com.example.snakelib.net.ServerGamePacket
import kotlinx.coroutines.delay
import java.time.Instant
import java.util.logging.Logger

class ServerGame(private val players: MutableList<Player>, private val map: String) {
    var active = false

    suspend fun runGame(startTime: Long) {
        if (players.isEmpty()) {
            return
        }

        val gameField = GameFieldLoader.loadGameField(map)!!
        val gamePlayers = players.mapIndexed { index, player ->
            val startPos = Vec2d(0.1f, (gameField.size.y / (players.size + 1)) * (index + 1))
            com.example.snakelib.Player(
                player.name, Snake(
                    player.color, mutableListOf(startPos), Vec2d(1.0, 0.0)
                )
            )
        }

        var tick = 0
        val runner = GameRunner(Game(gameField, gamePlayers.toMutableList()), true) { game ->
            var shouldSendGame = tick == 0 || true
            for (player in players) {
                for (packet in player.incoming) {
                    if (packet !is ClientPacket.GamePacket) {
                        continue
                    }
                    when (val gamePacket = packet.clientGamePacket) {
                        is ClientGamePacket.ChangeDirection -> {
                            game.getPlayer(player.name).snake.updateDirection(gamePacket.newDirection)
                            shouldSendGame = true
                        }
                    }
                }
                player.incoming.clear()
            }

            tick += 1

            if (shouldSendGame) {
                for (player in players) {
                    if (player.active) {
                        player.sendGamePacket(ServerGamePacket.UpdateGame(game))
                    }
                }
            }
        }

        delay(startTime - Instant.now().toEpochMilli())
        active = true
        runner.runGame()
        active = false

        Logger.getLogger("ServerGame").info("Game has ended!")

        for (player in players) {
            player.active = false
            player.listenJob?.cancel()
            player.listenJob?.join()
        }

        Logger.getLogger("ServerGame").info("All players exited")
    }
}