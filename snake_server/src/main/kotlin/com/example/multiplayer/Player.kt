package com.example.multiplayer

import com.example.snakelib.net.*
import io.ktor.server.websocket.*
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.ClosedReceiveChannelException
import java.util.logging.Logger

class Player(private val session: WebSocketServerSession) {
    lateinit var name: String
    var color = 0

    var active: Boolean = true
    var listenJob: Job? = null
    var incoming: MutableList<ClientPacket> = mutableListOf()

    /**
     * Connects to the player and returns the players preferred lobby id
     */
    suspend fun connect(): Int? {
        val packet = receivePacket()
        if (packet !is ClientPacket.LobbyPacket || packet.lobbyPacket !is ClientLobbyPacket.Connect) {
            throw Exception("Excepted connect packet, got $packet")
        }
        val lobbyPacket = (packet.lobbyPacket as ClientLobbyPacket.Connect)
        name = lobbyPacket.playerName
        color = lobbyPacket.color
        return lobbyPacket.lobbyId
    }

    suspend fun sendPacket(packet: ServerPacket) {
        session.sendSerialized(packet)
    }

    suspend fun sendLobbyPacket(lobbyPacket: ServerLobbyPacket) {
        sendPacket(ServerPacket.LobbyPacket(lobbyPacket))
    }

    suspend fun sendGamePacket(gamePacket: ServerGamePacket) {
        sendPacket(ServerPacket.GamePacket(gamePacket))
    }

    suspend fun listen() {
        while (active) {
            val packet = try {
                receivePacket()
            } catch (e: ClosedReceiveChannelException) {
                active = false
                break
            }
            Logger.getLogger("Player").info("Got packet: $packet")
            incoming.add(packet)
        }
        Logger.getLogger("Player").info("Stopping packet listener for $name")
    }

    private suspend fun receivePacket(): ClientPacket {
        return session.receiveDeserialized()
    }
}