package com.example.plugins

import com.example.multiplayer.Lobbies
import com.example.multiplayer.Player
import com.example.multiplayer.ServerGame
import com.example.snakelib.net.ServerLobbyPacket
import io.ktor.serialization.kotlinx.*
import io.ktor.server.application.*
import io.ktor.server.routing.*
import io.ktor.server.websocket.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.serialization.json.Json
import java.time.Duration
import java.time.Instant
import java.util.logging.Logger

fun Application.configureSockets() {
    install(WebSockets) {
        pingPeriod = Duration.ofSeconds(15)
        timeout = Duration.ofSeconds(15)
        maxFrameSize = Long.MAX_VALUE
        masking = false
        contentConverter = KotlinxWebsocketSerializationConverter(Json)
    }

    val lobbies = Lobbies()
    val games = mutableListOf<ServerGame>()

    routing {
        webSocket("/ws") {
            val player = Player(this)
            var game: ServerGame? = null

            var id = player.connect()
            if (id === null) {
                id = lobbies.createLobby()
                if (id == null) {
                    player.sendLobbyPacket(ServerLobbyPacket.TooManyLobbies)
                    return@webSocket
                }
                launch {
                    val (startTime, map) = lobbies.getLobby(id)!!.run()
                    val lobby = lobbies.removeLobby(id)!!
                    game = ServerGame(lobby.players, map)
                    games.add(game!!)
                    game!!.runGame(startTime)
                }
            }
            val lobby = lobbies.getLobby(id)
            if (lobby !== null) {
                if (lobby.add(player)) {
                    player.sendLobbyPacket(
                        ServerLobbyPacket.Connected(
                            true, lobby.id, Instant.now().toEpochMilli()
                        )
                    )
                } else {
                    player.sendLobbyPacket(ServerLobbyPacket.PlayernameAlreadyExists)
                    player.active = false
                }
            } else {
                player.sendLobbyPacket(
                    ServerLobbyPacket.Connected(
                        false, 0, Instant.now().toEpochMilli()
                    )
                )
                player.active = false
            }

            player.listen()
            while (game?.active == true) {
                delay(100)
            }
            Logger.getLogger("Socket").info("Closing connection to ${player.name}")
        }
    }
}
