package com.example.plugins

import com.example.highscores.getHighscores
import com.example.highscores.saveHighscore
import com.example.snakelib.highscores.Highscore
import com.example.snakelib.net.Singleplayer
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.resources.*
import io.ktor.server.resources.post
import io.ktor.server.response.*
import io.ktor.server.routing.*
import org.jetbrains.exposed.sql.transactions.transaction

fun Application.configureRouting() {
    install(Resources)
    routing {
        post<Singleplayer.SubmitHighscore> {
            // TODO: Verify that name and map don't exceed their allowed length
            val highscore = call.receive<Highscore>()
            val success =
                transaction {
                    saveHighscore(Highscore(highscore.name, highscore.score, highscore.map))
                }
            call.respond("Success: $success")
        }
        get<Singleplayer.GetHighscores> {
            val highscores = transaction {
                getHighscores()
            }
            call.respond(highscores)
        }
    }
}
