package com.example.highscores

import com.example.snakelib.highscores.Highscore
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq

object Highscores : Table() {
    val name: Column<String> = varchar("name", 50)
    val score: Column<Int> = integer("score")
    val map: Column<String> = varchar("map", 50)
    override val primaryKey = PrimaryKey(name, map)
}

/**
 * Saves a highscore if there is not higher score for the same player name and map.
 * Returns whether the score was updated
 */
fun saveHighscore(highscore: Highscore): Boolean {
    val selection = (Highscores.name eq highscore.name) and (Highscores.map eq highscore.map)
    val prevHighscore =
        Highscores.select(selection)
            .firstOrNull()
    if (prevHighscore === null) {
        Highscores.insert {
            it[name] = highscore.name
            it[score] = highscore.score
            it[map] = highscore.map
        }
        return true
    } else if (prevHighscore[Highscores.score] <= highscore.score) {
        Highscores.update({ selection }) {
            it[score] = highscore.score
        }
        return true
    }

    return false
}


fun getHighscores() =
    Highscores.selectAll()
        .map { Highscore(it[Highscores.name], it[Highscores.score], it[Highscores.map]) }