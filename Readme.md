# SnakeArena

This repository contains the SnakeArena app and the SnakeArena server.

## Structure

Our project is separated into four directories: 
- app: The android app implementation
- snake_client: Mediator between the app and the snake server
- snake_server: The server program, not bundled with the app
- snakelib: Our data model, used by both the app and snake_server

## Getting started

To get started, follow these steps:
1. Clone this repository
2. Open the project in android studio
3. Sync gradle files and build the project
4. Start the dev server: Go to `snake_server/src/main/kotlin/Application.kt` and click the green arrow next to `fun main()`.
   This starts the webserver on your device.
5. Run the app on the android emulator. Android studio should automatically configure the launch configuration

When the app is build in debug mode, it will try to connect to the server on your local device. When the app is build in release mode, it will try to connect to a vm hosted by the NTNU. Note that this machine is only reachable from within the network of the NTNU. The ip addresses for that are hardcoded at `app/src/main/java/com/example/snakegame/game/client/MultiplayerClient.kt`.

To test the app on a real device, set the debug mode ip address to the ip address of the device that you are running the server on.