# Pull convention

- Use branches for each feature
- Name convention: feature -> "feature/<feature_name>, fix -> "fix/<fix_name>


# Code documentation

- Use describing name for classes, methods and variables
- Use comments to desribe code
- Use code styles
