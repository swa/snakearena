package com.example.snakegame

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.snakegame.databinding.FragmentSettingsBinding
import com.example.snakegame.game.Settings


class SettingsFragment : Fragment() {

    private var _binding: FragmentSettingsBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {

        _binding = FragmentSettingsBinding.inflate(inflater, container, false)

        val currentColor = Settings.readColorFromFile(this.requireContext()) ?: Color.BLUE
        when (currentColor) {
            Color.YELLOW -> binding.yellow.isChecked = true
            Color.GREEN -> binding.green.isChecked = true
            Color.BLUE -> binding.blue.isChecked = true
            Color.RED -> binding.red.isChecked = true
        }

        val currentSound = Settings.readSoundFromFile(this.requireContext()) ?: true
        if (currentSound) {
            binding.sound.isChecked = true
        }
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //button to go back to the menu
        binding.back.setOnClickListener {
            findNavController().navigate(R.id.action_settingsFragment_to_MenuFragment)
        }
        //change the snake color in the settings.csv file
        binding.color.setOnCheckedChangeListener { _, colorId ->
            val button: RadioButton = binding.root.findViewById(colorId)
            var color: Int = Color.BLUE
            when (button.text) {
                "Red" -> color = Color.RED
                "Yellow" -> color = Color.YELLOW
                "Green" -> color = Color.GREEN
            }
            Settings.writeColorToFile(this.requireContext(), color)
        }

        //activate or deactivate the sound
        binding.sound.setOnCheckedChangeListener { _, boolean ->
            Settings.writeSoundToFile(this.requireContext(), boolean)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}