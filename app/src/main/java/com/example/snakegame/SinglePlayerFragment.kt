package com.example.snakegame

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.snakegame.databinding.FragmentSinglePlayerBinding
import com.example.snakegame.game.Settings
import com.example.snakegame.game.client.GameMode
import com.example.snakegame.game.client.SingleplayerClient


class SinglePlayerFragment : Fragment() {

    private var _binding: FragmentSinglePlayerBinding? = null
    private var username: String? = null


    lateinit var radioButton: RadioButton

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        username = Settings.readNameFromFile(this.requireContext())
        _binding = FragmentSinglePlayerBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.name.setText(username)


        binding.enterGameButton.setOnClickListener {
            username = binding.name.text.toString()
            Settings.writeNameToFile(this.requireContext(), username!!)

            val mapId = binding.mapChoice.checkedRadioButtonId
            radioButton = binding.root.findViewById(mapId)
            val map = radioButton.text.toString()


            SingleplayerClient.setGame(requireContext(), map)
            (context as BaseActivity).navigateToGameActivity(GameMode.Singleplayer)
        }

        //button to go back to the menu
        binding.back.setOnClickListener {
            findNavController().navigate(R.id.action_singlePlayerFragment_to_MenuFragment)

        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}