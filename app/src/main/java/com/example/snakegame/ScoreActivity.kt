package com.example.snakegame

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.lifecycleScope
import com.example.snakegame.databinding.ActivityScoreBinding
import com.example.snakegame.game.Highscores
import com.example.snakegame.game.client.GameMode
import com.example.snakegame.game.client.GameMode.Companion.toGameMode
import com.example.snakegame.game.client.SingleplayerClient
import com.example.snakelib.highscores.Highscore
import kotlinx.coroutines.launch


class ScoreActivity : BaseActivity() {
    private lateinit var binding: ActivityScoreBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityScoreBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val map = intent.getStringExtra("MAP")!!
        val gameMode = intent.getIntExtra("GAME_MODE", 0).toGameMode()!!

        if (gameMode != GameMode.Singleplayer) {
            binding.restartButton.visibility = View.INVISIBLE
        }

        lifecycleScope.launch {
            val name = intent.getStringExtra("PLAYER_NAME")

            val yourHighscore = if (name !== null) {
                Highscores.read(name, map)?.score
            } else {
                null
            }

            val serverHighscores: List<Highscore> = Highscores.readLeaderboard(map)
            binding.yourHisghscore.text = "$yourHighscore"
            binding.mapTitle.text = getString(R.string.mapname, map)

            for (i in 0..4) {
                if (serverHighscores.size > i) {
                    val bestscore = serverHighscores.get(i).score
                    val bestname = serverHighscores.get(i).name
                    val text = binding.leaderboard.text
                    binding.leaderboard.text =
                        getString(R.string.bestplayer, text, bestname, bestscore)
                }
            }
        }

        val score = intent.getIntExtra("SCORE", 0)
        binding.yourScore.text = "$score"

        binding.restartButton.setOnClickListener {
            SingleplayerClient.setGame(this, map)
            navigateToGameActivity(gameMode)
            finish()
        }

        binding.menuButton.setOnClickListener {
            val mainMenu = Intent(this, MainActivity::class.java)
            startActivity(mainMenu)
            finish()
        }
    }
}

