package com.example.snakegame

import android.os.Bundle
import androidx.core.view.WindowCompat
import androidx.lifecycle.lifecycleScope
import com.example.snakegame.databinding.ActivityMainBinding
import com.example.snakegame.game.Settings
import com.example.snakegame.game.client.MultiplayerClient
import kotlinx.coroutines.launch

class MainActivity : BaseActivity() {
    //    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        WindowCompat.setDecorFitsSystemWindows(window, false)
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        Settings.initFile(this)

        lifecycleScope.launch {
            MultiplayerClient.reset()
        }
//        val navController = findNavController(R.id.nav_host_fragment_content_main)
//        appBarConfiguration = AppBarConfiguration(navController.graph)
//        setupActionBarWithNavController(navController, appBarConfiguration)

    }

}