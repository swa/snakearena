package com.example.snakegame

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.snake_client.ClientStage
import com.example.snakegame.databinding.FragmentMultiplayerMenuBinding
import com.example.snakegame.game.Settings
import com.example.snakegame.game.client.MultiplayerClient
import kotlinx.coroutines.launch


class MultiplayerMenuFragment : Fragment() {
    private var _binding: FragmentMultiplayerMenuBinding? = null
    private var username: String? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        username = Settings.readNameFromFile(this.requireContext())
        _binding = FragmentMultiplayerMenuBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.name.setText(username)

        binding.createGameButton.setOnClickListener {
            username = binding.name.text.toString()
            Settings.writeNameToFile(this.requireContext(), username!!)

            lifecycleScope.launch {
                connectToLobby(null)
                MultiplayerClient.snakeClient?.stageChangeListener = {
                    if (it is ClientStage.LobbyStage) {
                        findNavController().navigate(R.id.action_multiplayerMenuFragment_to_hostWaitFragment)
                        true
                    } else {
                        false
                    }
                }
            }
        }

        // Make sure that the entered code is valid before allowing a player to join
        binding.enterCode.addTextChangedListener {
            val parsedCode = binding.enterCode.text.toString().toIntOrNull()
            binding.joinGameButton.isEnabled = parsedCode !== null && parsedCode >= 0
        }

        binding.joinGameButton.setOnClickListener {
            username = binding.name.text.toString()
            Settings.writeNameToFile(this.requireContext(), username!!)

            val id = binding.enterCode.text.toString().toInt()
            lifecycleScope.launch {
                connectToLobby(id)
                MultiplayerClient.snakeClient?.stageChangeListener = {
                    if (it is ClientStage.LobbyStage) {
                        findNavController().navigate(R.id.action_multiplayerMenuFragment_to_guestWaitFragment)
                        true
                    } else {
                        false
                    }
                }
            }
        }

        //button to go back to the menu
        binding.back.setOnClickListener {
            findNavController().navigate(R.id.action_multiplayerMenuFragment_to_MenuFragment)

        }
    }

    private suspend fun connectToLobby(lobbyId: Int?) {
        MultiplayerClient.reset()
        val defaultStage = MultiplayerClient.snakeClient?.defaultStage ?: return

        // TODO: always return color from Settings
        val color = Settings.readColorFromFile(requireContext()) ?: -256
        defaultStage.connect(binding.name.text.toString(), lobbyId, color)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}