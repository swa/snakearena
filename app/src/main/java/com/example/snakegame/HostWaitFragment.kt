package com.example.snakegame

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.snake_client.ClientStage
import com.example.snakegame.databinding.FragmentHostWaitBinding
import com.example.snakegame.game.Settings
import com.example.snakegame.game.client.GameMode
import com.example.snakegame.game.client.MultiplayerClient
import com.example.snakelib.net.ClientLobbyPacket
import kotlinx.coroutines.launch


class HostWaitFragment : Fragment() {
    private var _binding: FragmentHostWaitBinding? = null
    private var username: String? = null


    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {

        username = Settings.readNameFromFile(this.requireContext())
        _binding = FragmentHostWaitBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.startGameButton.setOnClickListener {
            findNavController().navigate(R.id.action_hostWaitFragment_to_gameActivity)
        }

        val pinCode = MultiplayerClient.snakeClient?.lobbyStage?.lobbyId
        binding.pinCode.text = "${pinCode ?: "error"}"

        binding.startGameButton.setOnClickListener {
            val mapId = binding.mapChoice.checkedRadioButtonId
            val radioButton: RadioButton = binding.root.findViewById(mapId)
            val map: String = radioButton.text.toString()

            lifecycleScope.launch {
                MultiplayerClient.snakeClient?.sendLobbyPacket(ClientLobbyPacket.StartGame(map))
                MultiplayerClient.snakeClient?.stageChangeListener = {
                    if (it is ClientStage.GameStage) {
                        (context as BaseActivity).navigateToGameActivity(GameMode.Multiplayer)
                        true
                    } else {
                        false
                    }
                }
            }
        }

        //button to go back to the menu
        binding.back.setOnClickListener {
            findNavController().navigate(R.id.action_hostWaitFragment_to_MenuFragment)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}