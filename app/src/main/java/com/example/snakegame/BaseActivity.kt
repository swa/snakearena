package com.example.snakegame

import android.content.Intent
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.snake_client.ServerError
import com.example.snakegame.game.client.GameMode
import com.example.snakegame.game.client.MultiplayerClient
import com.google.android.material.snackbar.Snackbar

open class BaseActivity : AppCompatActivity() {
    override fun onResume() {
        super.onResume()

        MultiplayerClient.errorHandler = {
            Log.e("SnakeClient", "An error occurred: $it")
            val message = when (it) {
                ServerError.ConnectionRefused -> getString(R.string.server_error_connection_refused)
                ServerError.InvalidGamePin -> getString(R.string.server_error_invalid_game_pin)
                ServerError.NotConnected -> getString(R.string.server_error_not_connected)
                ServerError.TooManyClients -> getString(R.string.server_error_too_many_clients)
                ServerError.PlayernameAlreadyExists -> getString(R.string.server_error_playername_already_exists)
            }
            Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_SHORT)
                .show()
        }
    }


    fun navigateToGameActivity(gameMode: GameMode) {
        val intent = Intent(this, GameActivity::class.java)
        intent.putExtra("multiplayer", gameMode.toInt())
        startActivity(intent)
    }
}