package com.example.snakegame.game

import android.content.Context
import android.graphics.Color
import java.io.File
import java.io.FileReader
import java.io.FileWriter

object Settings {
    private const val filename = "settings.csv"

    /**
     * inits file in file empty or not existing
     */
    fun initFile(context: Context) {
        val file = File(context.filesDir, filename)

        if (!file.exists()) {
            file.createNewFile()
        }
        val fr = FileReader(file)
        val string = fr.readText().split(";")
        fr.close()
        if (string.size < 3) {
            val fw = FileWriter(file)

            val color: String = Color.BLUE.toString()
            fw.write("New cool player;$color;true")

            fw.close()
        }
    }

    /**
     * Reads the Players Name from the File
     */
    fun readNameFromFile(context: Context): String? {
        val file = File(context.filesDir, filename)
        if (file.exists() && file.canRead()) {
            val fr = FileReader(file)
            val string = fr.readText().split(";")
            fr.close()
            return string[0]
        } else {
            return null
        }
    }

    /**
     * reads the stored color from the file
     */
    fun readColorFromFile(context: Context): Int? {
        val file = File(context.filesDir, filename)
        if (file.exists()) {
            val fr = FileReader(file)
            val string = fr.readText().split(";")
            fr.close()
            return string[1].toInt()
        } else {
            return null
        }
    }

    /**
     * reads the sound from the file
     */
    fun readSoundFromFile(context: Context): Boolean? {
        val file = File(context.filesDir, filename)
        if (file.exists()) {
            val fr = FileReader(file)
            val string = fr.readText().split(";")
            fr.close()
            return string[2].toBoolean()
        } else {
            return null
        }
    }

    /**
     * replaces the name in the File
     */
    fun writeNameToFile(context: Context, name: String) {
        val file = File(context.filesDir, filename)

        val fr = FileReader(file)
        val string = fr.readText().split(";")
        fr.close()

        val fw = FileWriter(file)
        fw.write(name + ";" + string[1] + ";" + string[2])
        fw.close()
    }


    /**
     * replaces the color in the file
     */
    fun writeColorToFile(context: Context, color: Int) {
        val file = File(context.filesDir, filename)

        val fr = FileReader(file)
        val string = fr.readText().split(";")
        fr.close()

        val fw = FileWriter(file)
        fw.write(string[0] + ";" + color.toString() + ";" + string[2])
        fw.close()
    }

    /**
     * replaces the sound on/off in the file
     */
    fun writeSoundToFile(context: Context, sound: Boolean) {
        val file = File(context.filesDir, filename)

        val fr = FileReader(file)
        val string = fr.readText().split(";")
        fr.close()

        val fw = FileWriter(file)
        fw.write(string[0] + ";" + string[1] + ";" + sound.toString())
        fw.close()
    }
}