package com.example.snakegame.game

import android.graphics.Canvas
import android.graphics.Paint
import com.example.snakelib.Snake
import com.example.snakelib.Vec2d

class Snake(private val inner_snake: Snake) {
    fun draw(canvas: Canvas, view: com.example.snakelib.Rect) {
        if (inner_snake.path.size < 2) {
            return
        }

        val trans = { pos: Vec2d ->
            view.relativePos(pos) * Vec2d(canvas.width.toFloat(), canvas.height.toFloat())
        }

        val paint = Paint().apply {
            style = Paint.Style.FILL
            strokeWidth = 16f
        }
        paint.color = inner_snake.color

        for ((first, second) in inner_snake.path.windowed(2)) {
            val (firstX, firstY) = trans(first)
            val (secondX, secondY) = trans(second)
            canvas.drawLine(firstX, firstY, secondX, secondY, paint)
        }
    }
}