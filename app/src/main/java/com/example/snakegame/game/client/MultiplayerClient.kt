package com.example.snakegame.game.client

import com.example.snake_client.ServerError
import com.example.snake_client.SnakeClient
import com.example.snakegame.BuildConfig
import com.example.snakelib.Game
import com.example.snakelib.Snake
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.coroutines.coroutineContext

object MultiplayerClient : GameClient {
    var snakeClient: SnakeClient? = null
    private var job: Job? = null

    var errorHandler: ((ServerError) -> Unit)? = null

    private val serverIp = if (BuildConfig.DEBUG) {
        "10.0.2.2"
    } else {
        "10.212.26.133"
    }

    suspend fun reset() {
        if (job !== null) {
            snakeClient?.join()
            job?.join()
        }

        snakeClient = SnakeClient(serverIp) { error ->
            errorHandler?.let { it(error) }
        }
        job = CoroutineScope(coroutineContext).launch { snakeClient?.run() }

        while (snakeClient !== null && !snakeClient!!.connected) {
            delay(10)
        }
    }

    override fun launchGame() {
        snakeClient?.gameStage?.launchGame()
    }

    override suspend fun join() {
        snakeClient?.gameStage?.onUpdate = null
        snakeClient?.join()
    }

    override fun onUpdate(handler: (Game) -> Unit) {
        snakeClient?.gameStage?.onUpdate = handler
    }

    override fun getPlayerId(): Int {
        return snakeClient?.gameStage?.playerId ?: 0
    }

    override fun getGame(): Game? {
        return snakeClient?.gameStage?.game
    }

    override suspend fun updateDirection(newDirection: Snake.DirectionChange) {
        snakeClient?.gameStage?.updateDirection(newDirection)
    }

    override fun setPaused(paused: Boolean): Boolean {
        return false
    }
}