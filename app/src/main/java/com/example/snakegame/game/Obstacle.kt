package com.example.snakegame.game

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import com.example.snakelib.Rect
import com.example.snakelib.Vec2d

class Obstacle(private val inner_obstacle: com.example.snakelib.Obstacle) {
    fun draw(canvas: Canvas, view: Rect) {
        val trans = { pos: Vec2d ->
            view.relativePos(pos) * Vec2d(canvas.width.toFloat(), canvas.height.toFloat())
        }

        val paint = Paint().apply {
            color = Color.WHITE
            style = Paint.Style.FILL
            strokeWidth = 16f
        }

        val translatedRect = Rect(
            trans(inner_obstacle.dimensions.start),
            trans(inner_obstacle.dimensions.end)
        )
        canvas.drawRect(
            translatedRect.left - 4,
            translatedRect.top - 4,
            translatedRect.right + 4,
            translatedRect.bottom + 4,
            paint
        )
    }
}
