package com.example.snakegame.game.client

import android.content.Context
import com.example.snakegame.game.Settings
import com.example.snakelib.*
import com.example.snakelib.game_field.GameFieldLoader
import kotlinx.coroutines.*

object SingleplayerClient : GameClient {
    private val runner = GameRunner(null, true) { internalOnGameUpdate(it) }
    private var onUpdate: ((Game) -> Unit)? = null
    var job: Job? = null
    var isPaused: Boolean = false

    fun setGame(context: Context, map: String) {
        val username = Settings.readNameFromFile(context)!!
        val color = Settings.readColorFromFile(context)!!

        val field = GameFieldLoader.loadGameField(map)!!
        val startPos = Vec2d(0.1f, field.size.y / 2)
        val player = Player(username, Snake(color, mutableListOf(startPos), Vec2d(1.0, 0.0)))
        job?.cancel()
        runner.game = Game(field, listOf(player))
    }

    override fun launchGame() {
        job = CoroutineScope(Dispatchers.Default).launch {
            runner.runGame()
        }
    }

    override suspend fun join() {
        job?.cancel()
        runner.game = null
        onUpdate = null
        isPaused = false
    }

    override fun onUpdate(handler: (Game) -> Unit) {
        onUpdate = handler
    }

    override fun getPlayerId(): Int {
        return 0
    }

    override fun getGame(): Game? {
        return runner.game
    }

    override suspend fun updateDirection(newDirection: Snake.DirectionChange) {
        runner.game?.players?.get(getPlayerId())?.snake?.updateDirection(newDirection)
    }

    override fun setPaused(paused: Boolean): Boolean {
        isPaused = paused
        return paused
    }

    private suspend fun internalOnGameUpdate(game: Game) {
        while (isPaused) {
            delay(100)
        }
        onUpdate?.let { it(game) }
    }
}