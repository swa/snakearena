package com.example.snakegame.game.client

import com.example.snakelib.Game
import com.example.snakelib.Snake

interface GameClient {
    fun launchGame()
    suspend fun join()

    fun onUpdate(handler: (Game) -> Unit)

    fun getPlayerId(): Int
    fun getGame(): Game?
    suspend fun updateDirection(newDirection: Snake.DirectionChange)

    // Tries to pause the game and returns whether the game is now paused
    fun setPaused(paused: Boolean): Boolean

}
