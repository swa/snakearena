package com.example.snakegame.game

import android.content.Context
import android.content.res.Resources
import android.graphics.Canvas
import android.util.AttributeSet
import android.util.Log
import android.view.View
import com.example.snakegame.R
import com.example.snakegame.game.client.GameMode
import com.example.snakelib.Game
import com.example.snakelib.Player
import com.example.snakelib.Rect
import com.example.snakelib.Vec2d

// Currently only works inside of a game activity
class GameView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0,
) : View(context, attrs, defStyleAttr) {

    private var _gameMode: GameMode = GameMode.Singleplayer
    var gameMode
        get() = _gameMode
        set(value) {
            _gameMode = value
            invalidate()
            requestLayout()
        }

    private val viewWidth: Float
    private val viewHeight: Float

    init {
        // Set the view dimensions into the game
        // The smaller dimensions should be one unit big
        val screenSize = Resources.getSystem().displayMetrics
        if (screenSize.widthPixels < screenSize.heightPixels) {
            viewWidth = 1.0f
            viewHeight = screenSize.heightPixels.toFloat() / screenSize.widthPixels.toFloat()
        } else {
            viewHeight = 1.0f
            viewWidth = screenSize.widthPixels.toFloat() / screenSize.heightPixels.toFloat()
        }

        // Retrieve the game mode from the passed attributes
        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.GameView,
            0, 0
        ).apply {
            try {
                val gameModeString = getInt(R.styleable.GameView_gameMode, 0)
                _gameMode = when (gameModeString) {
                    0 -> GameMode.Singleplayer
                    1 -> GameMode.Multiplayer
                    else -> throw IllegalArgumentException("Unknown game mode '$gameModeString'")
                }
            } finally {
                recycle()
            }
        }

    }

    override fun draw(canvas: Canvas) {
        super.draw(canvas)
        invalidate()

        val client = gameMode.getClient()
        val game = client.getGame() ?: return

        val viewRect = getViewRect(game, game.players[client.getPlayerId()])

        for (player in game.players) {
            Snake(player.snake).draw(canvas, viewRect)
        }

        for (obstacle in game.gameField.obstacles) {
            Obstacle(obstacle).draw(canvas, viewRect)
        }

        try {
            for (powerUp in game.gameField.powerUps.toList()) {
                PowerUp(powerUp, context).draw(canvas, viewRect)
            }
        } catch (e: ConcurrentModificationException) {
           Log.e("GameView", "ConcurrentModificationException occurred")
        }
    }

    private fun getViewRect(game: Game, player: Player): Rect {
        val centerPos = player.snake.path.last()
        val offset = Vec2d(viewWidth / 2f, viewHeight / 2f)
        val rect = Rect(centerPos - offset, centerPos + offset)

        if (rect.left < 0) {
            rect.right += -rect.left
            rect.left = 0f
        } else if (rect.right > game.gameField.size.x) {
            rect.left -= rect.right - game.gameField.size.x
            rect.right = game.gameField.size.x
        }

        if (rect.top < 0) {
            rect.bottom += -rect.top
            rect.top = 0f
        } else if (rect.bottom > game.gameField.size.y) {
            rect.top -= rect.bottom - game.gameField.size.y
            rect.bottom = game.gameField.size.y
        }

        // If it is impossible to fit the view rect within the map,
        // center the map in the view rect
        if (rect.left < 0 || rect.right > game.gameField.size.x) {
            val offsetX = (rect.right - rect.left - game.gameField.size.x) / 2f
            rect.left = -offsetX
            rect.right = game.gameField.size.x + offsetX
        }
        if (rect.top < 0 || rect.bottom > game.gameField.size.y) {
            val offsetY = (rect.bottom - rect.top - game.gameField.size.y) / 2f
            rect.top = -offsetY
            rect.bottom = game.gameField.size.y + offsetY
        }

        return rect
    }
}