package com.example.snakegame.game

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import com.example.snakelib.Rect
import com.example.snakelib.Vec2d
import com.example.snakelib.powerup.PowerUp


class PowerUp(private val inner_powerup: PowerUp, val context: Context) {
    @SuppressLint("DiscouragedApi")
    fun draw(canvas: Canvas, view: Rect) {
        val trans = { pos: Vec2d ->
            view.relativePos(pos) * Vec2d(canvas.width.toFloat(), canvas.height.toFloat())
        }

        val paint = Paint().apply {
            color = Color.parseColor(inner_powerup.getColor())
            style = Paint.Style.FILL
            strokeWidth = 16f
        }

        val translatedRect = Rect(
            trans(inner_powerup.dimensions.start),
            trans(inner_powerup.dimensions.end)
        )

        // takes image name and gets corresponding resource id out of res/drawable
        val resourceId = context.resources.getIdentifier(
            inner_powerup.getImageName(),
            "drawable",
            "com.example.snakegame"
        )
        // convert resource to bitmap
        val bitmap = BitmapFactory.decodeResource(context.resources, resourceId)
        // draw bitmap
        canvas.drawBitmap(
            bitmap,
            null,
            Rect(
                translatedRect.start.x.toInt(),
                translatedRect.start.y.toInt(),
                translatedRect.end.x.toInt(),
                translatedRect.end.y.toInt()
            ),
            paint
        )
    }
}