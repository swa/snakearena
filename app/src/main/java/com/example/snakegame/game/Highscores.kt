package com.example.snakegame.game

import com.example.snakegame.game.client.MultiplayerClient
import com.example.snakelib.highscores.Highscore

object Highscores {
    /**
     * Reads a highscore for a player with the game [name] and on the given [map]
     */
    suspend fun read(name: String, map: String): Highscore? {
        val highscores = MultiplayerClient.snakeClient?.getHighscores()
        return highscores?.firstOrNull { it.name == name && it.map == map }
    }

    suspend fun readLeaderboard(map: String): List<Highscore> {
        var highscores = MultiplayerClient.snakeClient?.getHighscores() ?: listOf()
        highscores = highscores.sortedBy { it.score }.reversed()
        return highscores.filter { it.map == map }.take(5)
    }

    /**
     * Writes a new highscore, if it is higher than the previous one
     */
    suspend fun write(highscore: Highscore) {
        MultiplayerClient.snakeClient?.addHighscore(highscore)
    }
}