package com.example.snakegame.game.client

enum class GameMode {
    Singleplayer,
    Multiplayer;

    fun getClient() = when (this) {
        Multiplayer -> MultiplayerClient
        Singleplayer -> SingleplayerClient
    }

    fun toInt() = when (this) {
        Singleplayer -> 0
        Multiplayer -> 1
    }

    companion object {
        fun Int.toGameMode() = when (this) {
            0 -> Singleplayer
            1 -> Multiplayer
            else -> null
        }
    }
}