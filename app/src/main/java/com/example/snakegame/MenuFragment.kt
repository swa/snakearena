package com.example.snakegame

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.snakegame.databinding.FragmentMenuBinding


class MenuFragment : Fragment() {

    private var _binding: FragmentMenuBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {

        _binding = FragmentMenuBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.version.text = BuildConfig.VERSION_NAME

        binding.singlePlayerButton.setOnClickListener {
            findNavController().navigate(R.id.action_FirstFragment_to_singlePlayerFragment)
        }
        binding.multiplayerButton.setOnClickListener {
            findNavController().navigate(R.id.action_FirstFragment_to_multiplayerMenuFragment)
        }
        binding.rulesButton.setOnClickListener {
            findNavController().navigate(R.id.action_FirstFragment_to_rulesFragment)
        }
        binding.settingsButton.setOnClickListener {
            findNavController().navigate(R.id.action_FirstFragment_to_settingsFragment)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}