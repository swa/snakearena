package com.example.snakegame

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.snake_client.ClientStage
import com.example.snakegame.databinding.FragmentGuestWaitBinding
import com.example.snakegame.game.Settings
import com.example.snakegame.game.client.GameMode
import com.example.snakegame.game.client.MultiplayerClient


class GuestWaitFragment : Fragment() {
    private var _binding: FragmentGuestWaitBinding? = null
    private var username: String? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {

        username = Settings.readNameFromFile(this.requireContext())
        _binding = FragmentGuestWaitBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val pinCode = MultiplayerClient.snakeClient?.lobbyStage?.lobbyId
        binding.pinCode.text = "${pinCode ?: "error"}"

        MultiplayerClient.snakeClient?.stageChangeListener = {
            if (it is ClientStage.GameStage) {
                (context as BaseActivity).navigateToGameActivity(GameMode.Multiplayer)
                true
            } else {
                false
            }
        }

        //button to go back to the menu
        binding.back.setOnClickListener {
            findNavController().navigate(R.id.action_guestWaitFragment_to_MenuFragment)
        }

    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}