package com.example.snakegame

import android.content.Intent
import android.content.res.Resources
import android.media.MediaPlayer
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import androidx.lifecycle.lifecycleScope
import com.example.snakegame.databinding.ActivityGameBinding
import com.example.snakegame.game.Highscores
import com.example.snakegame.game.Settings
import com.example.snakegame.game.client.GameClient
import com.example.snakegame.game.client.GameMode.Companion.toGameMode
import com.example.snakelib.Snake
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class GameActivity : BaseActivity() {
    lateinit var binding: ActivityGameBinding
    lateinit var client: GameClient

    private var mediaPlayer: MediaPlayer? = null
    private var isGamePaused: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityGameBinding.inflate(layoutInflater)
        setContentView(binding.root)

        Settings.initFile(this)

        val gameMode = intent.getIntExtra("multiplayer", 0).toGameMode()!!

        val sound: Boolean = Settings.readSoundFromFile(this) ?: true

        mediaPlayer = MediaPlayer.create(this, R.raw.gamesound)
        val crashSound = MediaPlayer.create(this, R.raw.bam)

        binding.buttonPauseMenu.setOnClickListener {
            pauseGame(true)
        }

        binding.buttonResume.setOnClickListener {
            pauseGame(false)
        }

        binding.buttonBackMenu.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }

        // if sound in the settings is activated play music during the game
        if (sound) {
            mediaPlayer!!.isLooping = true
            mediaPlayer!!.start()
        }

        client = gameMode.getClient()
        binding.gameCanvas.gameMode = gameMode

        var gameOver = false
        client.launchGame()
        client.onUpdate { game ->
            val player = game.players[client.getPlayerId()]
            if (!player.isAlive && !gameOver) {
                gameOver = true
                if (sound) {
                    crashSound.start()
                }

                val highscore = com.example.snakelib.highscores.Highscore(
                    player.name,
                    player.score,
                    game.gameField.name
                )
                lifecycleScope.launch {
                    Highscores.write(highscore)
                    client.join()
                    finish()
                }

                val displayScore = Intent(this@GameActivity, ScoreActivity::class.java)
                displayScore.putExtra("SCORE", player.score)
                displayScore.putExtra("PLAYER_NAME", player.name)
                displayScore.putExtra("MAP", game.gameField.name)
                displayScore.putExtra("GAME_MODE", gameMode.toInt())
                startActivity(displayScore)
            }

            lifecycleScope.launch {
                withContext(Dispatchers.Main) {
                    binding.scoreText.text =
                        resources.getString(R.string.score).format(player.score)
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        CoroutineScope(Dispatchers.Default).launch { client.join() }

        mediaPlayer!!.stop()
        mediaPlayer!!.release()
        mediaPlayer = null
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (event?.action != MotionEvent.ACTION_DOWN) {
            return false
        }

        val width = Resources.getSystem().displayMetrics.widthPixels
        val touchX = event.x

        val newDirection = when (touchX / width) {
            in 0f..0.5f -> Snake.DirectionChange.LEFT
            in 0.5f..1f -> Snake.DirectionChange.RIGHT
            else -> return false
        }

        lifecycleScope.launch {
            client.updateDirection(newDirection)
        }

        return true
    }


    /**
     * Handler method of the back-button of the navigation bar.
     * If <isGamePause> is false and back-button is pressed then pause view is shown.
     * if <isGamePause> is true and back-button is pressed then the pause view dissappear.
     */
    @Deprecated("Deprecated in Java")
    override fun onBackPressed() {
        if (!isGamePaused) {
            pauseGame(true)
        } else {
            pauseGame(false)
        }
    }

    /**
     * Handler method for the pause button in the game view.
     */
    private fun pauseGame(gamePaused: Boolean) {
        isGamePaused = gamePaused
        client.setPaused(isGamePaused)

        if (gamePaused) {
            // Show pause menu
            showPauseView()
        } else {
            // Hide pause menu
            hidePauseView()
        }
    }

    /**
     * Method makes the pause view visible.
     */
    private fun showPauseView() {
        binding.pauseMenu.visibility = View.VISIBLE

        binding.buttonPauseMenu.visibility = View.INVISIBLE
    }

    /**
     * Method hides the pause view.
     */
    private fun hidePauseView() {
        binding.pauseMenu.visibility = View.INVISIBLE

        binding.buttonPauseMenu.visibility = View.VISIBLE
    }
}