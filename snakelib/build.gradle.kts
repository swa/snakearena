val ktor_version: String by project
val exposedVersion: String by project

plugins {
    id("org.jetbrains.kotlin.jvm")
    kotlin("plugin.serialization") version "1.8.20"
}

dependencies {
    implementation("io.ktor:ktor-serialization-kotlinx-json-jvm:$ktor_version")
    implementation("io.ktor:ktor-client-resources:$ktor_version")
}