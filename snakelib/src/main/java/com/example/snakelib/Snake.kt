package com.example.snakelib

import kotlinx.serialization.Serializable

@Serializable
data class Snake(var color: Int, val path: MutableList<Vec2d>, var direction: Vec2d) {
    private var directionChanged = false
    var speed: Float = 0.004f

    /**
     * Moves the snake by one step into the given direction
     * stores the corner points of the lines into path
     */
    fun update() {
        if (directionChanged) {
            path.add(nextPos())
            directionChanged = false
        } else {
            if (path.size > 1) {
                path[path.lastIndex] = nextPos()
            } else {
                path.add(nextPos())
            }
        }
    }

    fun nextPos() = path.last() + direction * speed

    /**
     * changes the direction vector according to the (RIGHT/LEFT) input
     */
    fun updateDirection(dirChange: DirectionChange) {
        val oldX = direction.x
        when (dirChange) {
            DirectionChange.RIGHT -> {
                direction.x = -direction.y
                direction.y = oldX
                directionChanged = true
            }
            DirectionChange.LEFT -> {
                direction.x = direction.y
                direction.y = -oldX
                directionChanged = true
            }
        }
//        val angle = when (dirChange) {
//            DirectionChange.RIGHT -> PI / 2
//            DirectionChange.LEFT -> -PI / 2
//        }
//        val newX = cos(angle) * direction.x - sin(angle) * direction.y
//        val newY = sin(angle) * direction.x + cos(angle) * direction.y
//        direction = Vec2d(newX, newY)
//        directionChanged = true
    }

    /**
     * deletes the whole snake path and starts at it's origin
     */
    fun deletePath() {
        val startPosition: Vec2d = path.first()
        path.clear()
        path.add(startPosition)
    }

    enum class DirectionChange {
        RIGHT, LEFT
    }
}



