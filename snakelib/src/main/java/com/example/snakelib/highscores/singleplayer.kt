package com.example.snakelib.highscores

import kotlinx.serialization.Serializable

@Serializable
data class Highscore(val name: String, val score: Int, val map: String)