package com.example.snakelib

import kotlinx.serialization.Serializable

import com.example.snakelib.powerup.PowerUp


/**
 * This class is part of the model package. It contains all necessary data to handle a player
 * as an entity.
 *
 * @param name: name of the player in the game
 * @param snake: snake entity of the player
 */
@Serializable
data class Player(var name: String, val snake: Snake) {
    var score: Int = 0
    var isAlive: Boolean = true
    val powerUps = mutableListOf<PowerUp>()
}