package com.example.snakelib

import kotlinx.serialization.Serializable

@Serializable
data class Rect(var start: Vec2d, var end: Vec2d) {
    var left
        get() = start.x
        set(value) {
            start.x = value
        }
    var right
        get() = end.x
        set(value) {
            end.x = value
        }

    var top
        get() = start.y
        set(value) {
            start.y = value
        }
    var bottom
        get() = end.y
        set(value) {
            end.y = value
        }

    /**
     * Returns the relative position of the [point] inside of this rectangle
     */
    fun relativePos(point: Vec2d) = (point - start) / (end - start)

    /**
     * Returns whether this rect collides with a line defined by its [start] and [end] points
     */
    fun collidesWithLine(start: Vec2d, end: Vec2d): Boolean {
        val lines = listOf(
            Vec2d(left, top),
            Vec2d(right, top),
            Vec2d(right, bottom),
            Vec2d(left, bottom),
            Vec2d(left, top)
        )
        for (line in lines.windowed(2)) {
            val (line_start, line_end) = line
            if (checkLineCollision(start, end, line_start, line_end)) {
                return true
            }
        }
        return false
    }

    companion object {
        /**
         * Parses a [Rect] from a string, given the format: `<start_x> <start_y> <end_x> <end_y>`
         *
         * Returns null if the string is not a valid rectangle
         */
        fun parse(string: String): Rect? {
            val points = string.split(" ").mapNotNull { it.toFloatOrNull() }

            if (points.size != 4) {
                return null
            }

            val (start_x, start_y, end_x, end_y) = points
            return Rect(Vec2d(start_x, start_y), Vec2d(end_x, end_y))
        }

        /**
         * Returns whether two lines collide
         */
        fun checkLineCollision(
            first_start: Vec2d,
            first_end: Vec2d,
            second_start: Vec2d,
            second_end: Vec2d,
        ): Boolean {
            val u = first_end - first_start
            val v = second_end - second_start
            val p = first_start
            val q = second_start

            if (u.x / v.x == u.y / v.y) {
                return false
            }

            val r: Float
            val s: Float

            if (u.x == 0f) {
                r = (p.x - q.x) / v.x
                s = (q.y - p.y + r * v.y) / u.y
            } else if (u.y == 0f) {
                r = (p.y - q.y) / v.y
                s = (q.x - p.x + r * v.x) / u.x
            } else if (v.x == 0f) {
                r = (q.x - p.x) / u.x
                s = (p.y - q.y + r * u.y) / v.y
            } else if (v.y == 0f) {
                r = (q.y - p.y) / u.y
                s = (p.x - q.x + r * u.x) / v.x
            } else {
                r = (p.x + (q.x * u.y) / u.x - (p.x * u.y) / u.y - q.y) / (v.y - (v.x * u.y) / u.x)
                s = (q.x - p.x + r * v.x) / u.x
            }

            return r in 0f..1f && s in 0f..1f
        }
    }
}
