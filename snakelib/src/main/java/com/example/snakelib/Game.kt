package com.example.snakelib

import com.example.snakelib.game_field.GameField
import kotlinx.serialization.Serializable
import com.example.snakelib.powerup.PowerUpFactory
import java.util.logging.Logger
import kotlin.random.Random

@Serializable
data class Game(val gameField: GameField, val players: List<Player>) {
    companion object {
        const val TICK_LENGTH_MILLIS = 20
        const val MIN_POWERUP_DELAY = 4 * 1000 / TICK_LENGTH_MILLIS
        const val MAX_POWERUP_DELAY = 10 * 1000 / TICK_LENGTH_MILLIS
    }

    private var timeCount: Int = 0
    private var maxTimeCount: Int = MAX_POWERUP_DELAY

    val isRunning get() = players.any { it.isAlive }

    fun update(isServer: Boolean): Int {
        for (player in players) {
            if (player.isAlive) {
                player.snake.update()
                player.score++
            } else {
                player.snake.deletePath()
            }
        }

        if (isServer) {
            detectCollisions()
            handlePowerUps()
        }

        return 0
    }

    /**
     * produces new powerUps after a certain time and increases the time counts on the screen and effect time of the powerUps
     * that are on screen or applied to a player.
     */
    private fun handlePowerUps() {
        // produces random powerUp every 2 to 4 seconds
        if (timeCount >= maxTimeCount) {
            producePowerUp()
            timeCount = 0
            maxTimeCount = Random.nextInt(MIN_POWERUP_DELAY, MAX_POWERUP_DELAY)
        }

        timeCount += 1

        // increases onScreenTime and effectTime about every second
        if (timeCount % (1000 / TICK_LENGTH_MILLIS) == 0) {
            for (powerUp in gameField.powerUps) {
                powerUp.onScreenTime++
            }
            for (player in players) {
                for (powerUp in player.powerUps) {
                    powerUp.effectTime++
                }
            }
        }

        // removes all not caught powerUps and resets the effect of powerUps, if they exceed their duration
        removeTimeExceedingPowerUps()
    }

    fun getPlayer(name: String) = players.first { it.name == name }

    /**
     * adds a random powerUp to the game field.
     */
    private fun producePowerUp() {
        val powerUpFactory = PowerUpFactory()
        Logger.getLogger("Game").info("power up is generated...")
        gameField.powerUps.add(powerUpFactory.generatePowerUp())
    }

    /**
     * Removes all the powerUps that have been on the screen for a certain time and not picked up by a player.
     * It also resets the effect of a powerUp on a player if applicable.
     */
    private fun removeTimeExceedingPowerUps() {
        gameField.powerUps.retainAll { it.onScreenTime < 9 }

        for (player in players) {
            player.powerUps.retainAll {
                if (it.effectTime >= it.getDuration()) {
                    it.reset(player)
                    false
                } else {
                    true
                }
            }
        }
    }

    private fun detectCollisions() {
        // collision with player or obstacles
        for (player in players) {
            if (detectPlayerCollisions(player)) {
                player.isAlive = false
            }
        }

        // collisions with powerUps
        for (player in players) {
            handlePowerUpCollisions(player)
        }
    }

    private fun detectPlayerCollisions(player: Player): Boolean {
        if (player.snake.path.size < 1) {
            return false
        }

        // collisions with obstacles
        val start = player.snake.path.last()
        val end = player.snake.nextPos()
        for (obstacle in gameField.obstacles) {
            if (obstacle.dimensions.collidesWithLine(start, end)) {
                return true

            }
        }

        // collision with player itself
        for (otherPlayer in players) {
            val dropSegments = if (otherPlayer === player) {
                2
            } else {
                0
            }
            for ((other_end, other_start) in otherPlayer.snake.path.reversed().drop(dropSegments)
                .windowed(2)) {
                if (Rect.checkLineCollision(start, end, other_start, other_end)) {
                    return true
                }
            }
        }

        return false
    }

    /**
     * detects the collision of a snake with a powerUp and if so, applies the effect on the player.
     */
    private fun handlePowerUpCollisions(player: Player) {
        if (player.snake.path.size >= 2) {
            // collisions with powerUps
            val (end, start) = player.snake.path.reversed().take(2)
            gameField.powerUps.retainAll {
                if (it.dimensions.collidesWithLine(start, end)) {
                    Logger.getLogger("Game").info("player got a powerUp")
                    player.powerUps.add(it)
                    it.apply(player)
                    false
                } else {
                    true
                }
            }

        }
    }

}