package com.example.snakelib

import kotlinx.serialization.Serializable

@Serializable
data class Obstacle(val dimensions: Rect)
