package com.example.snakelib.game_field

import com.example.snakelib.Obstacle
import com.example.snakelib.Vec2d
import kotlinx.serialization.Serializable
import com.example.snakelib.powerup.PowerUp

@Serializable
class GameField internal constructor(
    val name: String,
    val size: Vec2d,
    val obstacles: List<Obstacle>,
    val powerUps: MutableList<PowerUp> = mutableListOf()
)