package com.example.snakelib.game_field

import com.example.snakelib.Obstacle
import com.example.snakelib.Rect
import com.example.snakelib.Vec2d

object GameFieldLoader {
    /**
     * Loads a [GameField] with the given [name].
     * If the field is not found or the file is invalid, null is returned
     */
    fun loadGameField(name: String): GameField? {
        val file = GameFieldLoader.javaClass.getResourceAsStream("/game_fields/$name.field")
        val lines = file?.bufferedReader()?.lines() ?: return null

        return parseField(name, lines.iterator())
    }

    private fun parseField(name: String, lines: Iterator<String>): GameField? {
        if (!lines.hasNext()) {
            return null
        }
        val fieldSize = Vec2d.parse(lines.next()) ?: return null

        val obstacles = mutableListOf<Obstacle>()
        for (line in lines) {
            val rect = Rect.parse(line) ?: return null
            obstacles.add(Obstacle(rect))
        }

        return GameField(name, fieldSize, obstacles)
    }
}