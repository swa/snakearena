package com.example.snakelib.powerup

import com.example.snakelib.Rect
import com.example.snakelib.Vec2d
import kotlin.random.Random

class PowerUpFactory {

    /**
     * @return random powerUp (one from enum Type) with random position
     */
    fun generatePowerUp(): PowerUp {
        val type = Type.values()[Random.nextInt(0, Type.values().size)]
        val start = Vec2d(Random.nextDouble(0.0, 0.92), Random.nextDouble(0.0, 0.92))
        val end = Vec2d(start.x + 0.08, start.y + 0.08)
        val dimensions = Rect(start, end)
        return when (type) {
            Type.SPEED -> Speed(dimensions, 0, 0)
            Type.RESET -> Reset(dimensions, 0, 0)
        }
    }

}