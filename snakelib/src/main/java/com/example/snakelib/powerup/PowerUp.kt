package com.example.snakelib.powerup

import com.example.snakelib.Player
import com.example.snakelib.Rect
import kotlinx.serialization.Serializable


@Serializable
sealed class PowerUp {
    // start and end of the rectangle
    abstract val dimensions: Rect

    // counter: the time a powerUp stays on the screen before disappearing (when not picked up py a player)
    abstract var onScreenTime: Int

    // counter: the time a powerUp has effect on a player
    abstract var effectTime: Int

    // color of the rectangle
    abstract fun getColor(): String

    // name of the powerUp icon stored on res/drawable
    abstract fun getImageName(): String

    // the time a powerUp has effect on a player
    abstract fun getDuration(): Int

    // apply certain effect on the player
    abstract fun apply(player: Player)

    // reset effect after specified duration
    abstract fun reset(player: Player)
}