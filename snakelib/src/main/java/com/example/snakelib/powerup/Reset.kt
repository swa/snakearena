package com.example.snakelib.powerup

import com.example.snakelib.Player
import com.example.snakelib.Rect
import com.example.snakelib.Vec2d
import kotlinx.serialization.Serializable

@Serializable
data class Reset(
    override val dimensions: Rect, override var onScreenTime: Int,
    override var effectTime: Int
) : PowerUp() {
    override fun getColor(): String {
        return "#00ff00"
    }

    override fun getImageName(): String {
        return "reset"
    }

    override fun getDuration(): Int {
        return 1
    }

    override fun apply(player: Player) {
        val startPosition: Vec2d = player.snake.path.last()
        player.snake.path.clear()
        player.snake.path.add(startPosition)
    }

    override fun reset(player: Player) {

    }
}