package com.example.snakelib.powerup

enum class Type {
    SPEED,
    RESET
}