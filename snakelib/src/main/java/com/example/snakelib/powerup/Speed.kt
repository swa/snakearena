package com.example.snakelib.powerup

import com.example.snakelib.Player
import com.example.snakelib.Rect
import kotlinx.serialization.Serializable

@Serializable
data class Speed(
    override val dimensions: Rect, override var onScreenTime: Int,
    override var effectTime: Int
) : PowerUp() {
    override fun getColor(): String {
        return "#ff0000"
    }

    override fun getDuration(): Int {
        return 5
    }

    override fun getImageName(): String {
        return "snail"
    }

    override fun apply(player: Player) {
        player.snake.speed *= 0.5f
    }

    override fun reset(player: Player) {
        player.snake.speed *= 2
    }
}


