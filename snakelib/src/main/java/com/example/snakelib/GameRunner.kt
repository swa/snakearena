package com.example.snakelib

import kotlinx.coroutines.delay
import java.time.Instant
import java.util.logging.Logger

class GameRunner(
    var game: Game?,
    private val isServer: Boolean,
    var onUpdate: suspend ((Game) -> Unit) = {}
) {
    suspend fun runGame() {
        while ((game?.isRunning) != false) {
            val now = Instant.now().toEpochMilli()
            val game = game
            if (game !== null) {
                game.update(isServer)
                onUpdate(game)
            }

            delay(Game.TICK_LENGTH_MILLIS.toLong() - Instant.now().toEpochMilli() + now)
        }
        game?.let { onUpdate(it) }
        Logger.getLogger("ClientStage.GameStage").info("GameActivity: Game thread stopped")
    }
}