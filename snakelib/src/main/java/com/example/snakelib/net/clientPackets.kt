package com.example.snakelib.net

import com.example.snakelib.Snake
import kotlinx.serialization.Serializable


@Serializable
sealed class ClientPacket {
    @Serializable
    data class LobbyPacket(val lobbyPacket: ClientLobbyPacket) : ClientPacket()

    @Serializable
    data class GamePacket(val clientGamePacket: ClientGamePacket) : ClientPacket()
}

@Serializable
sealed class ClientLobbyPacket {
    @Serializable
    data class Connect(val playerName: String, val lobbyId: Int?, val color: Int) :
        ClientLobbyPacket()

    // TODO: Pass the chosen map
    @Serializable
    data class StartGame(val map: String) : ClientLobbyPacket()
}

@Serializable
sealed class ClientGamePacket {
    @Serializable
    data class ChangeDirection(val newDirection: Snake.DirectionChange) : ClientGamePacket()
}
