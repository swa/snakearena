package com.example.snakelib.net

import com.example.snakelib.Game
import kotlinx.serialization.Serializable

@Serializable
sealed class ServerPacket {
    @Serializable
    data class LobbyPacket(val lobbyPacket: ServerLobbyPacket) : ServerPacket()

    @Serializable
    data class GamePacket(val gamePacket: ServerGamePacket) : ServerPacket()
}

@Serializable
sealed class ServerLobbyPacket {
    @Serializable
    data class Connected(val success: Boolean, val lobbyId: Int, val serverTime: Long) :
        ServerLobbyPacket()

    @Serializable
    data class StartGame(val startTime: Long, val playerId: Int) : ServerLobbyPacket()

    @Serializable
    object TooManyLobbies : ServerLobbyPacket()

    @Serializable
    object PlayernameAlreadyExists: ServerLobbyPacket()
}

@Serializable
sealed class ServerGamePacket {
    @Serializable
    data class UpdateGame(val newGame: Game) : ServerGamePacket()
}

