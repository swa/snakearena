package com.example.snakelib.net

import io.ktor.resources.*

@Suppress("unused")
@Resource("/singleplayer")
class Singleplayer {
    @Resource("submit_highscore")
    class SubmitHighscore(val parent: Singleplayer = Singleplayer())

    @Resource("get_highscores")
    class GetHighscores(val parent: Singleplayer = Singleplayer())
}