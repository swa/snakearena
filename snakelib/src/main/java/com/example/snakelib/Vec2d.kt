package com.example.snakelib

import kotlinx.serialization.Serializable

@Serializable
data class Vec2d(var x: Float, var y: Float) {
    constructor(x: Double, y: Double) : this(x.toFloat(), y.toFloat())

    operator fun plus(other: Vec2d) = Vec2d(x + other.x, y + other.y)

    operator fun minus(other: Vec2d) = Vec2d(x - other.x, y - other.y)

    operator fun times(other: Vec2d) = Vec2d(x * other.x, y * other.y)

    operator fun times(other: Float) = Vec2d(x * other, y * other)

    operator fun div(other: Vec2d) = Vec2d(x / other.x, y / other.y)

    companion object {
        /**
         * Parses a [Vec2d] from a string with the format: `<x> <y>`
         *
         * x and y can be floating points numbers
         */
        fun parse(string: String): Vec2d? {
            val subStrings = string.split(" ")
            if (subStrings.size != 2) {
                return null
            }

            val (width_str, height_str) = subStrings
            val width = width_str.toFloatOrNull() ?: return null
            val height = height_str.toFloatOrNull() ?: return null
            return Vec2d(width, height)
        }
    }
}
