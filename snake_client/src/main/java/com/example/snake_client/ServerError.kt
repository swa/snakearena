package com.example.snake_client

enum class ServerError {
    ConnectionRefused,
    InvalidGamePin,
    NotConnected,
    TooManyClients,
    PlayernameAlreadyExists,
}