package com.example.snake_client

import com.example.snakelib.highscores.Highscore
import com.example.snakelib.net.*
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.plugins.resources.*
import io.ktor.client.plugins.websocket.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.serialization.*
import io.ktor.serialization.kotlinx.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.websocket.*
import kotlinx.coroutines.cancel
import kotlinx.coroutines.channels.ClosedReceiveChannelException
import kotlinx.serialization.json.Json
import java.net.ConnectException
import java.net.SocketException
import java.util.logging.Logger

class SnakeClient(private val host: String, internal val errorHandler: ((ServerError) -> Unit)) {
    private val client = HttpClient(CIO) {
        install(Resources)
        install(ContentNegotiation) {
            json()
        }
        install(WebSockets) {
            contentConverter = KotlinxWebsocketSerializationConverter(Json)
        }
        defaultRequest {
            host = this@SnakeClient.host
            port = 8080
            url { protocol = URLProtocol.HTTP }
        }
    }

    var connected: Boolean = false
    internal var session: DefaultClientWebSocketSession? = null

    // Listeners for when the stage changes. If it returns true, it will be deleted from the listeners
    var stageChangeListener: ((ClientStage) -> Boolean)? = null
    private var stage: ClientStage = ClientStage.DefaultStage(this)

    val defaultStage get() = stage as? ClientStage.DefaultStage
    val lobbyStage get() = stage as? ClientStage.LobbyStage
    val gameStage get() = stage as? ClientStage.GameStage

    suspend fun run() {
        require(!connected) { "Client is already connected" }

        guardServerErrors {
            client.webSocket(method = HttpMethod.Get, host = host, port = 8080, path = "/ws") {
                connected = true
                session = this
                receive()
            }
        }
    }

    suspend fun getHighscores(): List<Highscore>? {
        return guardServerErrors {
            val response = client.get(Singleplayer.GetHighscores())
            response.body()
        }
    }

    suspend fun addHighscore(highscore: Highscore) {
        guardServerErrors {
            client.post(Singleplayer.SubmitHighscore()) {
                contentType(ContentType.Application.Json)
                setBody(highscore)
            }
        }
    }

    private suspend fun send(packet: ClientPacket) {
        if (!connected) {
            errorHandler(ServerError.NotConnected)
            return
        }

        Logger.getLogger("SnakeClient").info("Sending $packet")
        guardServerErrors {
            session?.sendSerialized(packet)
        }
    }

    suspend fun sendLobbyPacket(lobbyPacket: ClientLobbyPacket) =
        send(ClientPacket.LobbyPacket(lobbyPacket))

    suspend fun sendGamePacket(gamePacket: ClientGamePacket) = send(
        ClientPacket.GamePacket(
            gamePacket
        )
    )

    suspend fun join() {
        connected = false
        stage.join()
        session?.cancel()
        session = null
    }

    private suspend fun receive() {
        while (connected) {
            val message = guardServerErrors { session?.incoming?.receive() } ?: break
            val text = (message as? Frame.Text)?.readText()
            Logger.getLogger("SnakeClient").info("RECEIVED: $text")
            val packet: ServerPacket = session?.converter?.deserialize(message) ?: break
            Logger.getLogger("SnakeClient").info("Got $packet")
            handle(packet)
        }
    }

    private suspend fun handle(packet: ServerPacket) {
        val oldStage = stage
        stage = stage.handle(packet)

        if (oldStage !== stage) {
            val listener = stageChangeListener
            if (listener !== null && listener(stage)) {
                stageChangeListener = null
            }

            oldStage.join()
        }
    }

    private suspend fun <T> guardServerErrors(func: suspend () -> T): T? {
        try {
            return func()
        } catch (e: ConnectException) {
            errorHandler(ServerError.ConnectionRefused)
        } catch (_: ClosedReceiveChannelException) {
        } catch (_: SocketException) {
            errorHandler(ServerError.ConnectionRefused)
        }

        return null
    }
}