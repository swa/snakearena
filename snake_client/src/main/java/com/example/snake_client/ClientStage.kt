package com.example.snake_client

import com.example.snakelib.Game
import com.example.snakelib.GameRunner
import com.example.snakelib.Snake
import com.example.snakelib.net.*
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.time.Instant
import java.util.*

// TODO: Document the usage of this pattern (State pattern)
sealed class ClientStage(val client: SnakeClient) {
    abstract fun handle(packet: ServerPacket): ClientStage

    open suspend fun join() {}

    class DefaultStage(client: SnakeClient) : ClientStage(client) {
        override fun handle(packet: ServerPacket): ClientStage {
            val lobbyPacket = (packet as? ServerPacket.LobbyPacket)?.lobbyPacket ?: return this

            return when (lobbyPacket) {
                is ServerLobbyPacket.Connected -> {
                    if (!lobbyPacket.success) {
                        client.errorHandler(ServerError.InvalidGamePin)
                        client.connected = false
                        this
                    } else {
                        LobbyStage(
                            client,
                            lobbyPacket.lobbyId,
                            Instant.now().toEpochMilli() - lobbyPacket.serverTime
                        )
                    }
                }
                is ServerLobbyPacket.TooManyLobbies -> {
                    client.connected = false
                    client.errorHandler(ServerError.TooManyClients)
                    this
                }
                is ServerLobbyPacket.StartGame -> this
                is ServerLobbyPacket.PlayernameAlreadyExists -> {
                    client.errorHandler(ServerError.PlayernameAlreadyExists)
                    this
                }
            }
        }

        suspend fun connect(playerName: String, lobbyId: Int?, color: Int) {
            client.sendLobbyPacket(ClientLobbyPacket.Connect(playerName, lobbyId, color))
        }
    }

    class LobbyStage(client: SnakeClient, val lobbyId: Int, private val serverTimeOffsetMs: Long) :
        ClientStage(client) {
        override fun handle(packet: ServerPacket): ClientStage {
            val lobbyPacket = (packet as? ServerPacket.LobbyPacket)?.lobbyPacket ?: return this

            return when (lobbyPacket) {
                is ServerLobbyPacket.StartGame -> GameStage(
                    client,
                    lobbyPacket.playerId,
                    lobbyPacket.startTime,
                    serverTimeOffsetMs
                )
                is ServerLobbyPacket.Connected -> this
                is ServerLobbyPacket.TooManyLobbies -> this
                is ServerLobbyPacket.PlayernameAlreadyExists -> this
            }
        }
    }

    class GameStage(
        client: SnakeClient,
        val playerId: Int,
        private val startTime: Long,
        private val serverTimeOffsetMs: Long
    ) :
        ClientStage(client) {
        private val runner = GameRunner(null, false) { game -> onUpdate?.let { it(game) } }
        var onUpdate: ((Game) -> Unit)? = null

        private var gameJob: Job? = null

        val game get() = runner.game

        override fun handle(packet: ServerPacket): ClientStage {
            return when (val gamePacket = (packet as? ServerPacket.GamePacket)?.gamePacket) {
                is ServerGamePacket.UpdateGame -> {
                    runner.game = gamePacket.newGame
                    this
                }
                else -> {
                    throw Exception("Unhandled packet: $packet")
                }
            }
        }

        override suspend fun join() {
            gameJob?.cancel()
            gameJob?.join()
        }

        suspend fun updateDirection(directionChange: Snake.DirectionChange) {
            client.sendGamePacket(ClientGamePacket.ChangeDirection(directionChange))
        }

        fun launchGame() {
            require(gameJob === null) { "Game is already launched" }
            gameJob = client.session!!.launch {
                delay(startTime + serverTimeOffsetMs - Date().time)
                runner.runGame()
            }
        }
    }
}